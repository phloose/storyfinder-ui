module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true
    },
    "extends": [
        "airbnb-base",
        "prettier",
        "prettier/vue",
        "plugin:vue/essential",
        "plugin:node/recommended",
        "plugin:jest/recommended",
        "plugin:import/errors",
        "plugin:import/warnings"
    ],
    "plugins": [
        "prettier",
        "node",
        "jest",
        "import"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "settings": {
        "import/core-modules": [
            "electron",
            "vue"
        ],
        "import/resolver": {
            "node": {},
            "webpack": {
                "config": "./webpack.renderer.additions.js"
            }
        }
    },
    "ignorePatterns": [
        "src/python/**/*.js"
    ],
    "rules": {
        "prettier/prettier": [
            "error",
            {
                "endOfLine": "auto",
                "tabWidth": 4,
                "arrowParens": "avoid"
            }
        ],
        "node/no-unpublished-require": [
            "error",
            {
                "allowModules": [
                    "electron",
                    "vue",
                    "vue-devtools"
                ]
            }
        ],
        "node/no-unpublished-import": [
            "error",
            {
                "allowModules": [
                    "electron",
                    "vue",
                    "vue-nonreactive"
                ]
            }
        ],
        "node/no-unsupported-features/es-syntax": "off",
        "node/no-missing-require": [
            "error",
            {
                "resolvePaths": [
                    "./src/renderer/",
                    "./src/renderer/storyfinder"
                ]
            }
        ],
        "node/no-missing-import": [
            "error",
            {
                "resolvePaths": [
                    "./src/renderer/",
                    "./src/renderer/storyfinder"
                ]
            }
        ],
        "import/no-named-as-default": "off",
        "import/no-named-as-default-member": "off",
        "import/no-extraneous-dependencies": [
            "error",
            {
                "devDependencies": true
            }
        ],
        "arrow-parens": [
            "error",
            "as-needed"
        ],
        "indent": [
            "error",
            4,
            {
                "SwitchCase": 1
            }
        ],
        "class-methods-use-this": "off",
        "func-names": "off",
        "no-plusplus": [
            "error",
            {
                "allowForLoopAfterthoughts": true
            }
        ],
        "no-console": "off",
        "no-param-reassign": [
            "error",
            {
                "props": false
            }
        ]
    }
}
