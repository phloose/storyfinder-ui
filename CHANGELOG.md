# Changelog

## [1.0.0-a1] - 2021-01-08


### Added

- VueJS for the UI as it greatly facilitates DOM-related manipulations.
- Global ``store`` module as single origin of truth for application state.
- Global ``EventBus`` for handling application wide events.
- Use Main.vue per storyfinder submodule as entry point that is then used in the
  renderer entrypoint
- Support for other CRS than WGS84 by using [Proj4Leaflet](https://kartena.github.io/Proj4Leaflet/)
- After a dataset is loaded it is now shown on the map to give visual feedback that
  loading was successful
- Optional ``labels`` property to LLegend component for showing labels alongside break
  values
- Possibility to export the results of a hotspot analysis as a new dataset in geojson
  format by using [FileSaver.js](https://github.com/eligrey/FileSaver.js/)
- Possibility to export the visualization of a hotspot analysis as PNG image by using
  [dom-to-image](https://github.com/tsayen/dom-to-image)
- HMoranResult.vue: Component for the result classification of the spatial
  autocorrelation statistics (Moran's Index). Contains now an image which
  illustrates the different values and a horizontal bar on which the actual morans value
  is drawn with a red vertical bar. This aims to facilitate the interpretation of
  the result of the spatial autocorrelation statistics. Also the text descriptions for
  the classified result have been simplified.

### Changed

- Rewrite core components to be compatible with VueJS:
  - ``Base`` components for general structure of the map, sidebar and sidebar content:
    - BaseContainer.vue
    - BaseItem.vue
    - BaseLoader.vue
    - BaseMain.vue
    - BaseTab.vue
  - ``LeafletJS`` components:
    - LLegend.vue
    - LMap.vue
    - LSidebar.vue
  - ``Vega`` component:
    - VChart.vue
  - ``hotspot`` module components:
    - containers:
      - HCompareContainer.vue
      - HHotspotContainer.vue
      - HLoadContainer.vue
      - HMoranContainer.vue
    - tabs:
      - HExploreItem.vue
      - HHelpItem.vue
- Rename markdown files of containers/tabs to match respective names
- Remove menu bar from main application window ([BrowserWindow.removeMenu()](https://www.electronjs.org/docs/api/browser-window#winremovemenu-linux-windows))
- Change title of "Spatial Autocorrelation" container to "Cluster tendency" and simplify
  the description.
- Move histogram to HLoadContainer
- Package a full python interpreter instead of using pyInstaller as the latter causes a
  lot of trouble regarding the search path for shared libraries. Furthermore it
  complicates debugging, as you cannot inspect errorneous code directly.

### Removed

- Removed old vanilla JavaScript implementation
- Removed separate HChartsTab.vue tab with item HChartsItem.vue and containers:
  - HAttributeHistogramContainer.vue
  - HZScoreHistogramContainer.vue

### Security

- Update dependencies to their newest versions


## [0.1.0-a3] - 2020-06-22

### Changed

- `env.py`:
  - Change platform dependent conditionals to include `darwin`
  - Change path entries for shared libraries to be platform dependent for `linux` (.so) and `darwin` (.dylib).
- `server.spec`: Change platform dependent conditionals to include `darwin`

### Fixed

- Revert commit [0cf9994](https://gitlab.com/phloose/storyfinder-ui/-/commit/0cf999483c81cfc1939b53ded08f8dd33a2955ff) as leaflet-splitmap when used as webpack module overwrites `L.control` which in turn has effects on other components like the leaflet-sidebar, that also extends `L.control`. Needs further investigation
- Fix building `storyfinder-ui` with electron-builder on MacOS by downgrading electron-builder to 21.2.0 (see: https://github.com/electron-userland/electron-builder/issues/4846)


## [0.1.0-a2] - 2020-06-20

### Added

- Add `darwin` tmp_map key in `py-update` script for installing/updating [StoryFinder python package](https://gitlab.com/phloose/storyfinder) on MacOS

### Fixed

- Fix using [leaflet-splitMap](https://github.com/QuantStack/leaflet-splitmap) without commenting out `require` calls to css files in index.js inside node_modules by using a fork from [github.com/phloose/leaflet-splitmap](https://github.com/phloose/leaflet-splitmap) that also publishes the webpack bundle.


## [0.1.0-a1] - 2020-06-16

### Added

- Building via [electron-webpack](https://webpack.electron.build/) and [electron-builder](https://www.electron.build/)
- Building python standalone via [PyInstaller](https://pyinstaller.readthedocs.io/en/stable/)
- Electron main process:
  - Communication with storyfinder backend server via `child_process`
  - Forward stdout and stderr to renderer process via `BrowserWindow.webcontents.send`
- Electon renderer:
  - `storyfinder` renderer package:
    - Communication with main process via `ipcRenderer`
    - Modules:
      - `bases`: Contains base classes for creating sidebar content
      - `utils`: Utility functions for various tasks
      - `maputils`: Utility functions for map related tasks
    - Geostatistics modules:
      - `hotspot`
        - `tabs`: Implementation of the user interactions for the single tabs
          - `explore`
          - `charts`
          - `help`
        - `containers`: TabContainer specializations/content for the single tabs
          - `explore`:
            - `load`: Description, file dialog and dropdown for loading a dataset and selecting an attribute
            - `moran`: Description and button for calculating Moran's Index for the selected attribute
            - `hotspot`: Description and button for performing a hotspot analysis for the selected attribute
            - `compare`: Description and form elements for displaying a [leaflet-splitMap](https://github.com/QuantStack/leaflet-splitmap) slider on the map that shows the numerical classification of an attribute using up to 15 classes, 3 [colorbrewer](https://colorbrewer2.org/#type=sequential&scheme=BuGn&n=3) color scales (YlGnBu, RdPu, YlOrBr) and 3 classification methods ([ckmeans](https://simplestatistics.org/docs/#ckmeans), [quantiles, equal](https://gka.github.io/chroma.js/#chroma-limits)) aside to the hotspot analysis result.
          - `charts`:
            - [Vega](https://vega.github.io/) histogram container for selected attribute and a button for showing/redrawing the vega chart
            - [Vega](https://vega.github.io/) histogram container for selected and analyzed attribute and a button for showing/redrawing the vega chart
          - `help`:
            - Help text for Moran's Index/Spatial Autocorrelation
            - Help text for hotspot analysis
            - Help text for numerical classification
  - Python backend server using Flask and [StoryFinder python package](https://gitlab.com/phloose/storyfinder)
      - Endpoints:
          - `load`: Load a dataset
          - `moran`: Calculate Moran's Index for a dataset
          - `hotspot`: Perform a hotspot analysis (Getis Ord) for a dataset
