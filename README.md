# StoryFinder

Perform geostatistical analysis without expert knowledge and heavy GIS-software.

## Intention

The main idea behind *StoryFinder* is to provide a simple tool that can be used to get first
insights into spatial datasets, maybe to find an interesting story to investigate further.

---
**NOTE**

Currently only polygon datasets can be analyzed.

---

## Installation

StoryFinder is supported on Windows, Linux and Mac.

### Pre-built installers

- [AppImage](https://nx8605.your-storageshare.de/s/CyE6zSFBB4Aq65i)
- [Windows-Installer](https://nx8605.your-storageshare.de/s/SR4yM6sRbP3KNL8)
- [Apple Disk Image (dmg)](https://nx8605.your-storageshare.de/s/MS7B8iLwkeS7sr5)

## Development

StoryFinder uses the [Electron](https://www.electronjs.org/) framework to be platform independent and be able to use JavaScript, [LeafletJS](https://leafletjs.com/) for the interactive map and a python backend that uses the [PySAL's ESDA package](https://pysal.org/esda/) (Exploratory Spatial Data Analysis) for the geostatistical analysis.

### Requirements

- [NodeJS/NPM](https://nodejs.org/en/download/)
- Python3
- [Git](https://git-scm.com/downloads)

At first the repository needs to be cloned:

```
git clone https://gitlab.com/phloose/storyfinder-ui
```

Once this is done proceed to the next step.

#### Installation of the JavaScript dependencies

Installation of the NodeJS/JavaScript dependencies is straight forward once NodeJS and NPM are installed:

```
cd storyfinder-ui
npm install
```

This may take a while especially for the electron binaries.

At the moment there is a problem with [leaflet-splitmap](https://github.com/QuantStack/leaflet-splitmap). Its bundle is build with [webpack](https://webpack.js.org/) but the node package contains the non-bundled version which tries to require css files and fails. A workaround for now is to uncomment lines 2 and 3 in `node_modules/leaflet-splitmap/src/index.js`.

#### Installation of the Python dependencies

To install the [storyfinder python package](https://gitlab.com/phloose/storyfinder) you need
[Miniconda](https://docs.conda.io/en/latest/miniconda.html) (or
[Anaconda](https://www.anaconda.com/distribution/) if already installed).
Especially on windows the installation of binary dependencies is complicated and error
prone, so conda is used to faciliate that. In anyway it is better to use a dedicated
environemt for a specific project.

Until a better solution is found we package a full python interpreter with the app. It
has shown that the first pyInstaller solution can result in hard to resolve bugs that
take a lot of time to debug.

In the ``src/python/py_interpreter`` directory create a new python environment:

* `cd src/python/`

* `conda create -p py_interpreter python=3.8 flask -c conda-forge --no-default-packages`

Then the dependencies of the storyfinder python package need to be installed in that
environment:

* Download the list of dependencies: `curl https://gitlab.com/phloose/storyfinder/-/raw/master/env.yml > /tmp/storyfinder-env.yml`
* Update the conda environment: `conda env update -p py_interpreter --file /tmp/storyfinder-env.yml`

Now storyfinder itself needs to be installed:

`./py_interpreter/python -m pip install --upgrade git+https://gitlab.com/phloose/storyfinder.git`

Alternatively the npm task py-update can be run:

`npm run py-update`

### Adding new Flask endpoints

All endpoints the python backend exposes are defined in [src/python/server.py](https://gitlab.com/phloose/storyfinder-ui/src/python/server.py). When a new
 endpoint needs to be added this is the place to go. Be sure to return a JSON with Flask's jsonify function.

### Development server

StoryFinder uses [electron-webpack](https://webpack.electron.build/) which contains a development server that allows for a tight feedback-loop between executing the app and making changes to the source code.

Be sure to have installed the python environment in `src/python/py_interpreter` or you will not be able to communicate with the python backend.

Then to start the development server run:

`npm run dev`

This will open the browser window in which StoryFinder runs. Any changes you make to the code while the dev server is running will be "watched" and will immediately reload StoryFinder user interface.

### Tests

For testing facebook's [jest](https://jestjs.io/) framework is used. They are found in
`__tests__/storyfinder` and are split into tests for the ``base`` components and the
``storyfinder`` modules (currently there is only the ``hotspot`` module). The ``storyfinder``
modules implement actual user interactions and should be tested for output as well as
specific functionality that is own to the particular component. Since the ``base``
components are tested separately and build up the basic structure for all other UI parts
it is not strictly necessary to test actual DOM output in the storyfinder modules.
Furthermore snapshot testing is used for the markup to test if it renders correctly. Most
of the ``storyfinder`` modules have very specific behaviour that interacts with third-party
libraries like [LeafletJS](https://leafletjs.com/) or
[Vega](https://vega.github.io/vega/) so the basic assumption is here that if used
correctly these libraries do their work. Therefore if appropriate and necessary mock
them out, as the specific setup for single components should not depend directly on them.

To run tests:

`npm run test:renderer`

When adding new functionality or fixing a bug be sure to add at least one test!

### Building from source

There are two npm tasks for building StoryFinder:

- `npm run dist` to build a packaged executable (AppImage on Linux, NSIS-Installer on Windows) which is saved in the created `dist/` directory
- `npm run dist:dir` to only assemble the electron binaries and the StoryFinder bundle into a folder in `dist/<platform>-unpacked` where `<platform>` is the either `windows` or `linux`. In this folder there is an executable named `storyfinder-ui`.

If errors occur it can sometimes help to delete the dist directories by using the npm
clean task `npm run clean`.

## Contribution

*StoryFinder-UI* is an
[ES6](https://en.wikipedia.org/wiki/ECMAScript#6th_Edition_%E2%80%93_ECMAScript_2015)
based project, which tries to use all modern JavaScript features and improvements.
Furthermore [VueJS](https://vuejs.org/) is used to facilitate UI development. When contributing code to
StoryFinder adhere to the below described module structure.

For writing larger text markdown files can be imported as a markdown-loader for webpack is used when compiling the bundle.

### Module structure

StoryFinder uses a module based structure. Normally no changes need to be made to the main electron process (`src/main`) so all development should be made towards the renderer process whose source code lives
in `src/renderer/`.

### Aliases

To work more efficiently with modules specific aliases have been created:

- storyfinder
- hotspot
- utils
- maputils

Instead of typing long relative paths these modules can be imported like so:

```
import BaseContainer from "storyfinder/components/BaseContainer.vue";
import LLegend from "storyfinder/components/leaflet/LLegend.vue";

import { classify, clearOptions, populateOptions } from "storyfinder/utils";
```

When creating a new geostistics module the alias for the module name needs to be declared in:

- webpack.renderer.additions.js -> resolve.alias property
- jest.config.js -> moduleNameMapper property

If using VSCode:

- jsconfig.json -> compilerOptions.paths property

### Description of the module structure

#### renderer

This the main entry point for the renderer process. It loads all webpack specific dependencies and the available StoryFinder modules.

#### storyfinder

The `storyfinder` module holds the actual application code. This is where new geostatistic modules should be placed. At the moment there is only a single geostatistics module named `hotspot`

#### storyfinder submodules: components, utils, maputils

- `components` contains base components (prefix Base*) that build up the UI including
  leaflet (prefix L*) and vega (prefix V*)
  specific components
- `utils` contains general utility functions
- `maputils` contains map related utility functions

#### New geostatistics modules

Every new geostatistics module consists of a certain structure:

```
.
└── storyfinder/
    └── new-geostatistics-module/
        ├── components/
        │   ├── containers/
        │   │   ├── Container1.vue
        |   |   ├── Container2.vue
        │   │   └── ...
        │   ├── md/
        │   │   ├── Container1.md
        |   |   ├── Container2.md
        │   │   └── ...
        │   ├── tabs/
        │   │   ├── Tab1.vue
        │   │   ├── Tab2.vue
        │   │   └── ...
        │   ├── Item1.vue
        │   ├── Item2.vue
        │   └── ...
        └── Main.vue
```

For an example have a look at the `storyfinder/hotspot` module

##### General structure

``Main.vue`` is the entrypoint for the ``hotspot`` module. It adds the ``tabs`` and
corresponding ``items`` to the menu sidebar. At the final step it is imported into the
renderer entry point and used to create a new Vue istance.

The ``tabs`` itself add the ``containers`` which themselfs contain the actual content for
the particular functionality of a module. The ``containers`` are where the magic happens and
content is displayed to the user.

It is planned to use [Vue Router](https://router.vuejs.org/) when more modules are added
to storyfinder.
