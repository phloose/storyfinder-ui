const jQ = jest.requireActual("jquery");

const getJSON = jest.fn(() => {
    return jQ.Deferred();
});

const ajax = jest.fn(() => {
    return jQ.Deferred();
});

export const $ = {
    ...jQ,
    getJSON,
    ajax,
};

export default $;
