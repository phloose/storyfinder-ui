const mockedL = jest.genMockFromModule("leaflet");

export const sidebarInstance = {
    addTo: jest.fn()
};
export const sidebar = jest.fn().mockReturnValue(sidebarInstance);
export const Sidebar = sidebar;

export const L = {
    ...mockedL,
    Control: {
        extend: jest.fn().mockReturnValue(Sidebar),
        Sidebar,
    },
    control: {
        sidebar: sidebar
    }
};

export default L;
