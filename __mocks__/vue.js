const Vue = jest.requireActual("vue");

Vue.nonreactive = jest.fn();

export default Vue;
