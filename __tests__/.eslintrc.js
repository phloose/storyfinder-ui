module.exports = {
    "rules": {
        "global-require": 0,
        "node/no-unpublished-require": [
            "error",
            {
                "allowModules": [
                    "@vue/test-utils"
                ]
            }
        ],
        "node/no-unpublished-import": [
            "error",
            {
                "allowModules": [
                    "vue",
                    "@vue/test-utils"
                ]
            }
        ]
    }
}
