import Vue from "vue";

import BaseContainer from "storyfinder/components/BaseContainer.vue";
import BaseLoader from "storyfinder/components/BaseLoader.vue";

import { getMountedComponent } from "../../../utils";

describe("BaseContainer", () => {
    test("renders correctly", () => {
        const comp = getMountedComponent(BaseContainer, {
            id: "test-container",
        });
        expect(comp.html()).toMatchSnapshot();
    });
    test("builds up generic container component", () => {
        const comp = getMountedComponent(BaseContainer, {
            id: "test-container",
        });
        // container rendererd correctly
        expect(comp.find("#test-container").exists()).toBe(true);
        // child divs have ids according to given id
        expect(comp.findComponent(BaseLoader).exists()).toBe(true);
        expect(comp.find("#test-container-textpane").exists()).toBe(true);
        expect(comp.find("#test-container-formpane").exists()).toBe(true);
        expect(comp.find("#test-container-msgpane").exists()).toBe(true);
        expect(comp.find("#test-container-errorpane").exists()).toBe(true);
    });
    test("loader should not be visible by default", () => {
        const comp = getMountedComponent(BaseContainer, {
            id: "test-container",
        });
        expect(comp.vm.loaderVisibility).toBe(false);
    });
    test("loaderAnimation sets loaderVisibility", () => {
        const comp = getMountedComponent(BaseContainer, {
            id: "test-container",
        });
        comp.vm.loaderAnimation(true);
        expect(comp.vm.loaderVisibility).toBe(true);
        comp.vm.loaderAnimation(false);
        expect(comp.vm.loaderVisibility).toBe(false);
    });
    test("loaderAnimation adds or removes 'active'-class to/from Loader css", async () => {
        const comp = getMountedComponent(
            BaseContainer,
            {
                id: "test-container",
            },
            false
        );
        comp.vm.loaderAnimation(true);
        await Vue.nextTick(() => {
            expect(comp.findComponent(BaseLoader).classes()).toContain(
                "active"
            );
        });
        comp.vm.loaderAnimation(false);
        await Vue.nextTick(() => {
            expect(comp.findComponent(BaseLoader).classes()).not.toContain(
                "active"
            );
        });
    });
    test("calling showError() shows an error in the errorpane for 3 seconds", () => {
        jest.useFakeTimers();
        const comp = getMountedComponent(BaseContainer, {
            id: "test-container",
        });
        comp.vm.showError("Foo");
        expect(comp.vm.$refs.errorpane.innerHTML).toContain("Foo");
        jest.advanceTimersByTime(3000);
        expect(comp.vm.$refs.errorpane.innerHTML).not.toContain("Foo");
    });
    test("calling showMessage() shows a message in the msgpane for 3 seconds", () => {
        jest.useFakeTimers();
        const comp = getMountedComponent(BaseContainer, {
            id: "test-container",
        });
        comp.vm.showMessage("Foo");
        expect(comp.vm.$refs.msgpane.innerHTML).toContain("Foo");
        jest.advanceTimersByTime(3000);
        expect(comp.vm.$refs.msgpane.innerHTML).not.toContain("Foo");
    });
    test("calling setMessage() sets a message in the msgpane", () => {
        const comp = getMountedComponent(BaseContainer, {
            id: "test-container",
        });
        comp.vm.setMessage("Foo");
        expect(comp.vm.$refs.msgpane.innerHTML).toContain("Foo");
    });
});
