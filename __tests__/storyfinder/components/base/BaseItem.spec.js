import BaseItem from "storyfinder/components/BaseItem.vue";
import TheGitlabItem from "storyfinder/components/TheGitlabItem.vue";

import { shallowMount, mount } from "@vue/test-utils";

describe("BaseItem", () => {
    test("renders correctly", () => {
        const comp = shallowMount(BaseItem, {
            propsData: {
                href: "someName",
                fontawesome: "fa bla",
            },
        });
        expect(comp.html()).toMatchSnapshot();
    });
    test("default", () => {
        const comp = shallowMount(BaseItem, {
            propsData: {
                href: "someName",
                fontawesome: "fa bla",
            },
        });
        expect(comp.find("a").attributes().href).toBe("#someName");
        expect(comp.find("a").attributes()).not.toContain("target");
        expect(comp.find("i").classes()).toContain("bla");
    });
    test("with address and external link", () => {
        const comp = shallowMount(BaseItem, {
            propsData: {
                href: "https://someaddress.xyz",
                fontawesome: "fa bla",
                external: true,
            },
        });
        expect(comp.find("a").attributes().href).toBe(
            "https://someaddress.xyz"
        );
        expect(comp.find("a").attributes().target).toBe("_blank");
        expect(comp.html()).toMatchSnapshot();
    });
});
describe("TheGitlabItem", () => {
    test("is setup properly", () => {
        const comp = mount(TheGitlabItem);
        expect(comp.find("a").attributes().href).toBe(
            "https://gitlab.com/phloose/storyfinder-ui"
        );
        expect(comp.find("a").attributes().target).toBe("_blank");
        expect(comp.find("i").classes()).toContain("fa-gitlab");
    });
});
