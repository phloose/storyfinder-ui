import Vue from "vue";

import BaseMain from "storyfinder/components/BaseMain.vue";
import LSidebar from "storyfinder/components/leaflet/LSidebar.vue";
import LMap from "storyfinder/components/leaflet/LMap.vue";

import { shallowMount, mount } from "@vue/test-utils";

jest.unmock("leaflet");

describe("Main", () => {
    describe("general structure", () => {
        let main;
        beforeEach(() => {
            main = shallowMount(BaseMain, {
                propsData: {
                    id: "test-main",
                },
            });
        });
        afterEach(() => {
            main.destroy();
        });
        test("renders correctly", () => {
            expect(main.html()).toMatchSnapshot();
        });
        test("contains a l-sidebar component with id='test-main'", () => {
            expect(main.findComponent(LSidebar).exists()).toBeTruthy();
            expect(main.findComponent(LSidebar).vm.id).toBe("test-main");
        });
        test("contains a l-map component with id='test-main'", () => {
            expect(main.findComponent(LMap).exists()).toBeTruthy();
            expect(main.findComponent(LMap).vm.id).toBe("test-main");
        });
    });
    describe("with slots", () => {
        let main;
        beforeEach(() => {
            main = mount(BaseMain, {
                propsData: {
                    id: "test-main",
                },
                slots: {
                    topitems: `<div id="topitems-dummy"></div>`,
                    bottomitems: `<div id="bottomitems-dummy"></div>`,
                    tabs: `<div id="tabs-dummy"></div>`,
                },
                attachToDocument: true,
            });
        });
        afterEach(() => {
            main.destroy();
        });
        test("renders slots correctly", () => {
            expect(main.find("#topitems-dummy").exists()).toBeTruthy();
            expect(main.find("#bottomitems-dummy").exists()).toBeTruthy();
            expect(main.find("#tabs-dummy").exists()).toBeTruthy();
        });
    });
    describe("communicates map-related events", () => {
        let main;
        beforeEach(() => {
            main = mount(BaseMain, {
                propsData: {
                    id: "test-main",
                },
                slots: {
                    topitems: `<div id="topitems-dummy"></div>`,
                    bottomitems: `<div id="bottomitems-dummy"></div>`,
                    tabs: `<div id="tabs-dummy"></div>`,
                },
                attachToDocument: true,
            });
        });
        afterEach(() => {
            main.destroy();
        });
        test("map-created", async () => {
            Vue.nextTick(function () {
                expect(main.emitted("map-created")).toBeTruthy();
            });
        });
        test("sidebar-created", async () => {
            Vue.nextTick(function () {
                expect(main.emitted("sidebar-created")).toBeTruthy();
            });
        });
    });
});
