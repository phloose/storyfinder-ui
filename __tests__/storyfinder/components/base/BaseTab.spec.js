import BaseTab from "storyfinder/components/BaseTab.vue";
import BaseContainer from "storyfinder/components/BaseContainer.vue";

import { shallowMount } from "@vue/test-utils";

describe("BaseTab", () => {
    test("renders correctly", () => {
        const tab = shallowMount(BaseTab, { propsData: { id: "test-tab" } });
        expect(tab.html()).toMatchSnapshot();
    });
    test("builds up generic tab structure", () => {
        const tab = shallowMount(BaseTab, { propsData: { id: "test-tab" } });
        // tab rendered correctly
        expect(tab.find("#test-tab").exists()).toBe(true);
        // heading is set according to 'id' and title-cased
        expect(tab.find(".sidebar-header").text()).toEqual("Test-tab");
        // default position is 'right'
        expect(tab.find(".sidebar-close").find("i").classes()).toContain(
            "fa-caret-right"
        );
    });
    test("with containers slot", () => {
        // Using Components in slots property works but only when they don't need any
        // props, i.e. you cannot intantiate them. Using this approach makes it possible:
        // https://github.com/vuejs/vue-test-utils/issues/41#issuecomment-327179174
        const tab = shallowMount(BaseTab, {
            propsData: { id: "test-tab" },
            slots: {
                containers: {
                    render(h) {
                        return h(BaseContainer, { props: { id: "test-tab" } });
                    },
                },
            },
        });
        expect(tab.findComponent(BaseContainer).exists()).toBeTruthy();
    });
});
