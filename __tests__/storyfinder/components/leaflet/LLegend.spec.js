import L from "leaflet";
import { shallowMount } from "@vue/test-utils";

import LLegend from "storyfinder/components/leaflet/LLegend.vue";
import { blueWhiteRed } from "storyfinder/hotspot/colors";
import store from "storyfinder/store";

describe("LLegend", () => {
    let wrapper;
    let LMapMock;
    let LControlMock;
    const LControlInstance = {
        addTo: jest.fn(),
        remove: jest.fn(),
    };
    beforeEach(() => {
        LMapMock = jest.fn();
        LControlMock = jest.fn(() => LControlInstance);
        L.control = LControlMock;
        wrapper = shallowMount(LLegend, {
            propsData: {
                title: "test-legend",
                map: LMapMock,
                breaks: [1, 2, 3, 4],
                colorScale: blueWhiteRed,
            },
        });
    });
    afterEach(() => {
        wrapper.destroy();
        jest.resetAllMocks();
    });
    test("renders correclty", () => {
        expect(wrapper.html()).toMatchSnapshot();
    });
    test("renders correctly with labels", () => {
        wrapper.setProps({
            labels: ["low", "medium", "high"],
        });
        expect(wrapper.html()).toMatchSnapshot();
    });
    describe("before mount", () => {
        test("instantiates a L.Control object", () => {
            expect(LControlMock).toHaveBeenCalledWith({
                position: "bottomleft",
            });
        });
        test("stores the legendControl in the LegendStore", () => {
            expect(store.LLegends.getLegend("test-legend")).toBe(
                LControlInstance
            );
        });
        test("legendControl.onAdd returns the components element", () => {
            expect(LControlInstance.onAdd()).toBe(wrapper.vm.$el);
        });
    });
    describe("on mount", () => {
        test("adds the legend instance to the given map", () => {
            expect(LControlInstance.addTo).toHaveBeenCalledWith(LMapMock);
        });
    });
    describe("before destroy", () => {
        test("removes the legend from the map", () => {
            const spy = jest.spyOn(LControlInstance, "remove");
            wrapper.destroy();
            expect(spy).toHaveBeenCalled();
        });
        test("deletes the legendControl from the LegendStore", () => {
            const spy = jest.spyOn(store.LLegends, "deleteLegend");
            expect(store.LLegends.getLegend("test-legend")).toBe(
                LControlInstance
            );
            wrapper.destroy();
            expect(spy).toHaveBeenCalledWith("test-legend");
            expect(store.LLegends.getLegend("test-legend")).toBe(undefined);
        });
    });
    describe("breaksComputed", () => {
        test("returns a list of legend items sorted from high values to low values", () => {
            expect(wrapper.vm.breaksComputed).toEqual([
                { id: 0, lower: 3, upper: 4, color: "#ff0000" },
                { id: 1, lower: 2, upper: 3, color: "#ffffff" },
                { id: 2, lower: 1, upper: 2, color: "#0000ff" },
            ]);
        });
        test("respects default precision for floating point numbers (2)", () => {
            wrapper.setProps({ breaks: [1.333123, 2.333654, 3.9876, 4.5678] });
            expect(wrapper.vm.breaksComputed).toEqual([
                { id: 0, lower: "3.99", upper: "4.57", color: "#ff0000" },
                { id: 1, lower: "2.33", upper: "3.99", color: "#ffffff" },
                { id: 2, lower: "1.33", upper: "2.33", color: "#0000ff" },
            ]);
        });
        test("respects given precision for floating point numbers", () => {
            wrapper.setProps({
                breaks: [1.333123, 2.333654, 3.9876, 4.5678],
                precision: 3,
            });
            expect(wrapper.vm.breaksComputed).toEqual([
                { id: 0, lower: "3.988", upper: "4.568", color: "#ff0000" },
                { id: 1, lower: "2.334", upper: "3.988", color: "#ffffff" },
                { id: 2, lower: "1.333", upper: "2.334", color: "#0000ff" },
            ]);
        });
        test("respects given labels", () => {
            wrapper.setProps({
                labels: ["low", "medium", "high"],
            });
            expect(wrapper.vm.breaksComputed).toEqual([
                {
                    id: 0,
                    lower: 3,
                    upper: 4,
                    color: "#ff0000",
                    label: "high",
                },
                {
                    id: 1,
                    lower: 2,
                    upper: 3,
                    color: "#ffffff",
                    label: "medium",
                },
                {
                    id: 2,
                    lower: 1,
                    upper: 2,
                    color: "#0000ff",
                    label: "low",
                },
            ]);
        });
        test("does not insert labels when label array.length != breaks.length - 1", () => {
            wrapper.setProps({
                labels: ["low", "medium", "high", "higher"],
            });
            expect(wrapper.vm.breaksComputed[0].label).toBe(undefined);
        });
    });
});
