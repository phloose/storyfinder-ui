import LMap from "storyfinder/components/leaflet/LMap.vue";
import { shallowMount } from "@vue/test-utils";

import L from "leaflet";

jest.unmock("leaflet");

describe("LMap", () => {
    let map;
    beforeEach(() => {
        map = shallowMount(LMap, {
            propsData: {
                id: "test-map",
            },
            attachToDocument: true,
        });
    });
    afterEach(() => {
        map.destroy();
    });
    test("renders correclty", () => {
        expect(map.html()).toMatchSnapshot();
    });
    describe("on mount", () => {
        test("sets LMap to L.Map instance", () => {
            expect(map.vm.LMap).toBeInstanceOf(L.Map);
        });
        test("sets LTileLayer to L.TileLayer instance", () => {
            expect(map.vm.LTileLayer).toBeInstanceOf(L.TileLayer);
        });
    });
});
