import { shallowMount } from "@vue/test-utils";
import { sidebarInstance as mockedSidebarInstance } from "leaflet";

import EventBus from "storyfinder/EventBus";

describe("LSidebar", () => {
    describe("on mount", () => {
        let sidebar;
        let LSidebar;
        jest.isolateModules(() => {
            LSidebar = require("storyfinder/components/leaflet/LSidebar.vue")
                .default;
        });
        beforeEach(() => {
            sidebar = shallowMount(LSidebar, {
                propsData: {
                    id: "test-sidebar",
                },
                attachToDocument: true,
            });
        });
        afterEach(() => {
            sidebar.destroy();
        });
        test("renders correclty", () => {
            expect(sidebar.html()).toMatchSnapshot();
        });
        test("sets LSidebar to L.Control.Sidebar instance", () => {
            expect(sidebar.vm.LSidebar).toEqual(mockedSidebarInstance);
        });
        test("adds sidebar on 'map-created' event", () => {
            const leafletMap = jest.fn();
            EventBus.$emit("map-created", leafletMap);
            expect(mockedSidebarInstance.addTo).toBeCalledWith(leafletMap);
        });
    });
    describe("sidebarOptions", () => {
        let sidebar;
        let LSidebar;
        jest.isolateModules(() => {
            jest.resetModules();
            jest.unmock("leaflet");
            LSidebar = require("storyfinder/components/leaflet/LSidebar.vue")
                .default;
        });
        afterEach(() => {
            sidebar.destroy();
        });
        test("default (right)", () => {
            sidebar = shallowMount(LSidebar, {
                propsData: {
                    id: "test-sidebar",
                },
                attachToDocument: true,
            });
            expect(sidebar.classes()).toContain("sidebar-right");
        });
        test("left", () => {
            sidebar = shallowMount(LSidebar, {
                propsData: {
                    id: "test-sidebar",
                    sidebarOptions: {
                        position: "left",
                    },
                },
                attachToDocument: true,
            });
            expect(sidebar.classes()).toContain("sidebar-left");
        });
        test("right", () => {
            sidebar = shallowMount(LSidebar, {
                propsData: {
                    id: "test-sidebar",
                    sidebarOptions: {
                        position: "right",
                    },
                },
                attachToDocument: true,
            });
            expect(sidebar.classes()).toContain("sidebar-right");
        });
    });
});
