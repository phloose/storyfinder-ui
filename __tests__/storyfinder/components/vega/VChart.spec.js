import { shallowMount } from "@vue/test-utils";
import * as vega from "vega";

import VChart from "storyfinder/components/vega/VChart.vue";

const vegaSpecA = JSON.parse(`{
    "$schema": "https://vega.github.io/schema/vega-lite/v4.json",
    "description": "A simple bar chart with embedded data.",
    "width": "container",
    "data": {
      "values": [
        {"a": "A", "b": 28}, {"a": "B", "b": 55}, {"a": "C", "b": 43},
        {"a": "D", "b": 91}, {"a": "E", "b": 81}, {"a": "F", "b": 53},
        {"a": "G", "b": 19}, {"a": "H", "b": 87}, {"a": "I", "b": 52}
      ]
    },
    "mark": "bar",
    "encoding": {
      "x": {"field": "a", "type": "nominal", "axis": {"labelAngle": 0}},
      "y": {"field": "b", "type": "quantitative"}
    }
  }`);

const vegaSpecB = JSON.parse(`{
    "$schema": "https://vega.github.io/schema/vega-lite/v4.json",
    "width": "container",
    "data": {
      "url": "data/movies.json"
    },
    "transform":[{
      "density": "IMDB Rating",
      "bandwidth": 0.3
    }],
    "mark": "area",
    "encoding": {
      "x": {
        "field": "value",
        "title": "IMDB Rating",
        "type": "quantitative"
      },
      "y": {
        "field": "density",
        "type": "quantitative"
      }
    }
  }`);

describe("VChart", () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallowMount(VChart, {
            propsData: {
                id: "test-vchart",
                spec: vegaSpecA,
                width: 300,
            },
        });
    });
    afterEach(() => {
        wrapper.destroy();
        jest.resetAllMocks();
    });
    test("renders correclty", () => {
        expect(wrapper.html()).toMatchSnapshot();
    });
    test("creates a vega view instance", () => {
        expect(wrapper.vm.vegaView).toBeInstanceOf(vega.View);
    });
    test("when specs change a new vega view is created", () => {
        const initialView = wrapper.vm.vegaView;
        wrapper.setProps({ spec: vegaSpecB });
        wrapper.vm.$nextTick(function () {
            expect(wrapper.vm.vegaView).not.toEqual(initialView);
        });
    });
    test("when specs change the old vega view is finalized", () => {
        const spy = jest.spyOn(wrapper.vm.vegaView, "finalize");
        wrapper.setProps({ spec: vegaSpecB });
        wrapper.vm.$nextTick(function () {
            expect(spy).toBeCalledTimes(1);
        });
    });
    test("when specs change method 'addVegaView' is called", () => {
        const spy = jest.spyOn(wrapper.vm, "addVegaView");
        wrapper.setProps({ spec: vegaSpecB });
        wrapper.vm.$nextTick(function () {
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });
    test("when width changes View.width() is called with the new width", () => {
        const spy = jest.spyOn(wrapper.vm.vegaView, "width");
        wrapper.setProps({ width: 123 });
        wrapper.vm.$nextTick(function () {
            expect(spy).toBeCalledWith(123);
        });
    });
    test("calling vm.addEventListener calls View.addEventListener", () => {
        const spy = jest.spyOn(wrapper.vm.vegaView, "addEventListener");
        wrapper.vm.addEventListener("click", function () {});
        wrapper.vm.$nextTick(function () {
            expect(spy).toHaveBeenCalled();
        });
    });
    test("calling vm.addSignalListener calls View.addSignalListener", () => {
        const spy = jest.spyOn(wrapper.vm.vegaView, "addSignalListener");
        wrapper.vm.addSignalListener("width", function () {});
        wrapper.vm.$nextTick(function () {
            expect(spy).toHaveBeenCalled();
        });
    });
});
