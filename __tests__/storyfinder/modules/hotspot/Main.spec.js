import { shallowMount } from "@vue/test-utils";

import Main from "storyfinder/hotspot/Main.vue";
import HExploreItem from "storyfinder/hotspot/components/HExploreItem.vue";
import HExploreTab from "storyfinder/hotspot/components/tabs/HExploreTab.vue";

import HHelpItem from "storyfinder/hotspot/components/HHelpItem.vue";
import HHelpTab from "storyfinder/hotspot/components/tabs/HHelpTab.vue";

jest.unmock("leaflet");

describe("Hotspot Main", () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallowMount(Main);
    });
    afterEach(() => {
        wrapper.destroy();
    });
    test("renders correctly", () => {
        expect(wrapper.html()).toMatchSnapshot();
    });
    describe("Items", () => {
        test("Explore-Item exists", () => {
            expect(wrapper.findComponent(HExploreItem).exists()).toBeTruthy();
        });
        test("Help-Item exists", () => {
            expect(wrapper.findComponent(HHelpItem).exists()).toBeTruthy();
        });
    });
    describe("Tabs", () => {
        test("Explore-Tab exists", () => {
            expect(wrapper.findComponent(HExploreTab).exists()).toBeTruthy();
        });
        test("Help-Tab exists", () => {
            expect(wrapper.findComponent(HHelpTab).exists()).toBeTruthy();
        });
    });
});
