/* eslint-disable jest/no-disabled-tests */
import * as chroma from "chroma-js";
import L from "leaflet";
import Vue from "vue";
import { mount } from "@vue/test-utils";

import HCompareContainer from "storyfinder/hotspot/components/containers/HCompareContainer.vue";
import EventBus from "storyfinder/EventBus";
import store from "storyfinder/store";
import * as maputils from "storyfinder/maputils";
import * as utils from "storyfinder/utils";

jest.unmock("leaflet");

const dataset = {
    type: "FeatureCollection",
    features: [
        {
            type: "Feature",
            properties: {
                a_attr: 2.0,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
        {
            type: "Feature",
            properties: {
                a_attr: 4.0,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
        {
            type: "Feature",
            properties: {
                a_attr: 6.0,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
        {
            type: "Feature",
            properties: {
                a_attr: 8.0,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
        {
            type: "Feature",
            properties: {
                a_attr: 10.0,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
        {
            type: "Feature",
            properties: {
                a_attr: 12.0,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
    ],
};

describe("HCompareContainer", () => {
    let wrapper;
    beforeEach(() => {
        wrapper = mount(HCompareContainer);
        store.dataset = dataset;
        store.analyzedDataset = dataset;
    });
    afterEach(() => {
        wrapper.destroy();
        jest.resetAllMocks();
        store.dataset = undefined;
        store.analyzedDataset = undefined;
    });
    test("renders correctly", () => {
        expect(wrapper.html()).toMatchSnapshot();
    });
    test("'Classify' button starts classificaton", () => {
        const spy = jest
            .spyOn(wrapper.vm, "classify")
            .mockImplementation(() => {});
        wrapper.find("#compare-button").trigger("click");
        expect(spy).toHaveBeenCalled();
        spy.mockRestore();
    });
    test("'Remove slider' button doesn't do anything in initial state", () => {
        const spy = jest
            .spyOn(wrapper.vm, "removeSlider")
            .mockImplementation(() => {});
        wrapper.find("#stop-compare-button").trigger("click");
        expect(spy).not.toHaveBeenCalled();
        spy.mockRestore();
    });
    describe("methods", () => {
        describe("classify", () => {
            let eventBusMock;
            let utilsClassifyMock;
            beforeEach(() => {
                eventBusMock = jest
                    .spyOn(EventBus, "$emit")
                    .mockImplementation(() => {});
                utilsClassifyMock = jest
                    .spyOn(utils, "classify")
                    .mockImplementation(() => [1, 2, 3, 4]);
                wrapper.vm.$refs.classes.value = "10";
                wrapper.vm.$refs.method.value = "equal";
                const attr = document.createElement("option");
                attr.value = "a_attr";
                wrapper.vm.$refs.attribute.appendChild(attr);
            });
            afterEach(() => {
                eventBusMock.mockRestore();
                utilsClassifyMock.mockRestore();
                wrapper.vm.$refs.attribute.remove(0);
            });
            test("sets instance attributes 'classes' to $refs.classes value", () => {
                wrapper.vm.classify();
                expect(wrapper.vm.classes).toEqual("10");
            });
            test("sets instance attributes 'method' to $refs.method value", () => {
                wrapper.vm.classify();
                expect(wrapper.vm.method).toEqual("equal");
            });
            test("sets instance attributes 'attribute' to $refs.attribute value", () => {
                wrapper.vm.classify();
                expect(wrapper.vm.attribute).toEqual("a_attr");
            });
            test("emits event 'dataset-classified' to the Eventbus", () => {
                wrapper.vm.classify();
                expect(eventBusMock).toHaveBeenCalledWith(
                    "dataset-classified",
                    [1, 2, 3, 4]
                );
            });
        });
        describe("onDatasetLoad", () => {
            let clearOptionsMock;
            let populateOptionsMock;
            const loaded = {
                dataset: jest.fn(),
                datasetPath: jest.fn(),
            };
            beforeEach(() => {
                clearOptionsMock = jest
                    .spyOn(utils, "clearOptions")
                    .mockImplementation(() => {});
                populateOptionsMock = jest
                    .spyOn(utils, "populateOptions")
                    .mockImplementation(() => {});
            });
            afterEach(() => {
                clearOptionsMock.mockRestore();
                populateOptionsMock.mockRestore();
            });
            test("calls utils.clearOptions to clear the attribute dropdown", () => {
                wrapper.vm.onDatasetLoad(loaded);
                expect(clearOptionsMock).toHaveBeenCalledWith(
                    expect.any(HTMLSelectElement),
                    false
                );
            });
            test("calls utils.populateOptions to fill the attribute dropdown", () => {
                wrapper.vm.onDatasetLoad(loaded);
                expect(populateOptionsMock).toHaveBeenCalledWith(
                    loaded.dataset,
                    expect.any(HTMLSelectElement)
                );
            });
        });
        describe("onClassified", () => {
            let wrapperB;
            let mapDiv;
            let sidebarDiv;
            let removeSplitMapMock;
            let addCompareDatasetMock;
            let addSplitMapMock;
            beforeEach(() => {
                wrapperB = mount(HCompareContainer, {
                    attachToDocument: true,
                });
                removeSplitMapMock = jest
                    .spyOn(wrapperB.vm, "removeLSplitMap")
                    .mockImplementation(() => {});
                addCompareDatasetMock = jest
                    .spyOn(wrapperB.vm, "addCompareDataset")
                    .mockImplementation(() => {});
                addSplitMapMock = jest
                    .spyOn(wrapperB.vm, "addSplitMap")
                    .mockImplementation(() => {});
                // Hacky mess: Fix hotspot-sidebar offsetWidth issue! See also tests for
                // HHotspotContainer.vue
                mapDiv = document.createElement("div");
                sidebarDiv = document.createElement("div");
                sidebarDiv.id = "hotspot-sidebar";
                document.body.appendChild(sidebarDiv);
                document.body.appendChild(mapDiv);
                store.LMap = L.map(mapDiv);
            });
            afterEach(() => {
                wrapperB.destroy();
                removeSplitMapMock.mockRestore();
                addCompareDatasetMock.mockRestore();
                addSplitMapMock.mockRestore();
            });
            test("if there is a splitmap existing removes it", () => {
                store.LSplitMap = jest.fn();
                wrapperB.vm.onClassified([1, 2, 3, 4]);
                expect(removeSplitMapMock).toHaveBeenCalled();
            });
            test("adds a compare dataset by calling vm.addCompareDataset", () => {
                wrapperB.vm.onClassified([1, 2, 3, 4]);
                // TODO: How to set a dropdowns value via code in vue component?
                expect(addCompareDatasetMock).toHaveBeenCalledWith({
                    attribute: expect.any(String),
                    colorscale: expect.any(String),
                    breaks: [1, 2, 3, 4],
                });
            });
            test("adds the splitMap control by calling vm.addSplitMap", () => {
                wrapperB.vm.onClassified([1, 2, 3, 4]);
                expect(addSplitMapMock).toHaveBeenCalledWith(
                    addCompareDatasetMock.mock.results[0].value,
                    expect.any(Number)
                );
            });
            test("creates a new LLegend component", () => {
                wrapperB.vm.onClassified([1, 2, 3, 4]);
                expect(wrapperB.vm.legend.$options.name).toEqual("LLegend");
            });
            test("the LLegend component constructor receives correct arguments", () => {
                wrapperB.vm.onClassified([1, 2, 3, 4]);
                expect(wrapperB.vm.legend.$options.propsData).toEqual({
                    title: expect.any(String),
                    map: store.LMap,
                    breaks: [1, 2, 3, 4],
                    colorScale: expect.any(Function),
                    precision: 3,
                });
            });
        });
        describe("addCompareDataset", () => {
            const attribute = "a_attr";
            const colorscale = "chromaColorScale";
            const breaks = [1, 2, 3, 4];

            beforeEach(() => {
                store.dataset = dataset;
                L.geoJSON = jest.fn(() => ({
                    addTo: jest.fn(),
                }));
                chroma.scale = jest.fn(() => ({
                    classes: jest.fn(),
                }));
            });
            afterEach(() => {
                store.dataset = undefined;
                L.geoJSON.mockRestore();
                chroma.scale.mockRestore();
            });
            // Chroma is imported via namespace import and that somehow  breaks the
            // tests. We skip them until it is assured that a default import won't break
            // any functionality in the respective component.
            test.skip("creates a chroma color scale for given scale", () => {
                wrapper.vm.addCompareDataset({ attribute, colorscale, breaks });
                expect(chroma.scale).toHaveBeenCalledWith(colorscale);
            });
            test.skip("applies the breaks to the created chroma colorscale", () => {
                wrapper.vm.addCompareDataset({ attribute, colorscale, breaks });
                expect(
                    chroma.scale.mock.results[0].value.classes
                ).toHaveBeenCalledWith(breaks);
            });
            test("calls L.geoJSON factory function with correct pane 'splitmap_right'", () => {
                wrapper.vm.addCompareDataset({ attribute, colorscale, breaks });
                expect(L.geoJSON).toHaveBeenCalledWith(store.dataset, {
                    pane: "splitmap_right",
                    name: "compare_dataset",
                    style: expect.any(Function),
                });
            });
            test("adds the L.GeoJSON instance to the map'", () => {
                store.LMap = jest.fn();
                wrapper.vm.addCompareDataset({ attribute, colorscale, breaks });
                expect(
                    L.geoJSON.mock.results[0].value.addTo
                ).toHaveBeenCalledWith(store.LMap);
            });
        });
        describe("addSplitMap", () => {
            let compareDataset;
            beforeEach(() => {
                compareDataset = dataset;
                store.mapDataset = dataset;
                L.control.splitMap = jest.fn(() => ({
                    addTo: jest.fn(),
                    _range: {
                        value: null,
                    },
                    _updateClip: jest.fn(),
                }));
            });
            afterEach(() => {
                compareDataset = undefined;
                store.mapDataset = undefined;
                L.control.splitMap.mockRestore();
            });
            test("calls the L.control.splitMap factory function", () => {
                wrapper.vm.addSplitMap(store.mapDataset, 123);
                expect(L.control.splitMap).toHaveBeenCalledWith(
                    store.mapDataset,
                    compareDataset
                );
            });
            test("adds the splitMap control instance to the map", () => {
                store.LMap = jest.fn();
                wrapper.vm.addSplitMap(store.mapDataset, compareDataset);
                expect(store.LSplitMap.addTo).toHaveBeenCalledWith(store.LMap);
            });
            test("returns the splitMap control instance", () => {
                const ret = wrapper.vm.addSplitMap(
                    store.mapDataset,
                    compareDataset
                );
                expect(ret).toBe(store.LSplitMap);
            });
        });
        describe("removeSlider", () => {
            let removeLSplitMapMock;
            beforeEach(() => {
                removeLSplitMapMock = jest
                    .spyOn(wrapper.vm, "removeLSplitMap")
                    .mockImplementation(() => {});
            });
            afterEach(() => {
                removeLSplitMapMock.mockRestore();
            });
            test("calls method removeLSplitMap", () => {
                wrapper.vm.removeSlider();
                expect(removeLSplitMapMock).toHaveBeenCalled();
            });
            test("disables 'Remove slider' button", () => {
                wrapper.vm.removeSlider();
                expect(wrapper.vm.$refs.stopbutton.disabled).toBe(true);
            });
        });
        describe("removeLSplitMap", () => {
            let removeGeoJSONMock;
            beforeEach(() => {
                store.LSplitMap = {
                    remove: jest.fn(),
                };
                removeGeoJSONMock = jest
                    .spyOn(maputils, "removeGeoJSON")
                    .mockImplementation(() => {});
            });
            afterEach(() => {
                store.LSplitMap = undefined;
                removeGeoJSONMock.mockRestore();
            });
            test("calls LSplitMap.remove()", () => {
                wrapper.vm.removeLSplitMap();
                expect(store.LSplitMap.remove).toHaveBeenCalled();
            });
            test("calls maputils.removeGeoJSON()", () => {
                store.LMap = jest.fn();
                wrapper.vm.removeLSplitMap();
                expect(removeGeoJSONMock).toHaveBeenCalledWith(
                    store.LMap,
                    "compare_dataset"
                );
            });
            test("when there is a LLegend component destroys it", () => {
                wrapper.vm.legend = new Vue();
                const spy = jest.spyOn(wrapper.vm.legend, "$destroy");
                wrapper.vm.removeLSplitMap();
                expect(spy).toHaveBeenCalled();
                spy.mockRestore();
            });
        });
    });
});
