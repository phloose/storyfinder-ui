import $ from "jquery";
import L from "leaflet";
import proj4 from "proj4";
import Vue from "vue";
import { mount } from "@vue/test-utils";

import HHotspotContainer from "storyfinder/hotspot/components/containers/HHotspotContainer.vue";
import EventBus from "storyfinder/EventBus";
import store from "storyfinder/store";
import * as maputils from "storyfinder/maputils";

jest.unmock("leaflet");
jest.mock("proj4");

const analyzedDataset = {
    type: "FeatureCollection",
    features: [
        {
            type: "Feature",
            properties: {
                a_attr: 2.0,
                z_sim_a_attr: 1.96,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
        {
            type: "Feature",
            properties: {
                a_attr: 4.0,
                z_sim_a_attr: 1.96,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
        {
            type: "Feature",
            properties: {
                a_attr: 6.0,
                z_sim_a_attr: 1.96,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
        {
            type: "Feature",
            properties: {
                a_attr: 8.0,
                z_sim_a_attr: 1.96,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
        {
            type: "Feature",
            properties: {
                a_attr: 10.0,
                z_sim_a_attr: 1.96,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
        {
            type: "Feature",
            properties: {
                a_attr: 12.0,
                z_sim_a_attr: 1.96,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
    ],
};

describe("HHotspotContainer", () => {
    let dfd;
    beforeEach(() => {
        dfd = $.Deferred();
        store.dataset = analyzedDataset;
    });
    afterEach(() => {
        jest.resetAllMocks();
        jest.restoreAllMocks();
    });
    test("renders correctly", () => {
        const wrapper = mount(HHotspotContainer);
        expect(wrapper.html()).toMatchSnapshot();
    });
    describe("loaderAnimation", () => {
        const EventBusEmit = EventBus.$emit;
        beforeAll(() => {
            EventBus.$emit = jest.fn();
        });
        afterAll(() => {
            EventBus.$emit = EventBusEmit;
        });
        test("is shown during request", () => {
            const wrapper = mount(HHotspotContainer);
            const spy = jest.spyOn(wrapper.vm.container, "loaderAnimation");
            $.getJSON.mockImplementation(() => {
                expect(spy).toHaveBeenCalledWith(true);
                return dfd.resolve(analyzedDataset);
            });
            wrapper.vm.analyzeDataset();
        });
        test("stops on success", () => {
            const wrapper = mount(HHotspotContainer);
            const spy = jest.spyOn(wrapper.vm.container, "loaderAnimation");
            $.getJSON.mockImplementation(() => dfd.resolve(analyzedDataset));
            wrapper.vm.analyzeDataset();
            expect(spy).toHaveBeenLastCalledWith(false);
        });
        test("stops on failure", () => {
            const wrapper = mount(HHotspotContainer);
            const spy = jest.spyOn(wrapper.vm.container, "loaderAnimation");
            $.getJSON.mockImplementation(() => dfd.reject());
            wrapper.vm.analyzeDataset();
            expect(spy).toHaveBeenLastCalledWith(false);
        });
    });
    describe("analyzeDataset", () => {
        const EventBusOn = EventBus.$on;
        beforeAll(() => {
            EventBus.$on = jest.fn();
        });
        afterAll(() => {
            EventBus.$on = EventBusOn;
        });
        test("on success emits event 'hotspots-analyzed' to the EventBus", () => {
            const wrapper = mount(HHotspotContainer);
            const spy = jest.spyOn(EventBus, "$emit");
            $.getJSON.mockImplementation(() => dfd.resolve(analyzedDataset));
            wrapper.vm.analyzeDataset();
            expect(spy).toHaveBeenCalledWith(
                "hotspots-analyzed",
                analyzedDataset
            );
        });
        test("on success enables 'Export result' button", async () => {
            const wrapper = mount(HHotspotContainer);
            $.getJSON.mockImplementation(() => dfd.resolve(analyzedDataset));
            expect(
                wrapper
                    .find("#hotspot-save-dataset-button")
                    .attributes("disabled")
            ).toBeTruthy();
            wrapper.vm.analyzeDataset();
            await Vue.nextTick();
            expect(
                wrapper
                    .find("#hotspot-save-dataset-button")
                    .attributes("disabled")
            ).toBeFalsy();
        });
        test("on success enables 'Export visualization' button", async () => {
            const wrapper = mount(HHotspotContainer);
            $.getJSON.mockImplementation(() => dfd.resolve(analyzedDataset));
            expect(
                wrapper.find("#hotspot-save-vis-button").attributes("disabled")
            ).toBeTruthy();
            wrapper.vm.analyzeDataset();
            await Vue.nextTick();
            expect(
                wrapper.find("#hotspot-save-vis-button").attributes("disabled")
            ).toBeFalsy();
        });
        test("on error calls containers showError method", () => {
            const wrapper = mount(HHotspotContainer);
            const spy = jest.spyOn(wrapper.vm.container, "showError");
            $.getJSON.mockImplementation(() => dfd.reject());
            wrapper.vm.analyzeDataset();
            expect(spy).toHaveBeenCalled();
        });
    });
    describe("onAnalyzed", () => {
        let mapDiv;
        let sidebarDiv;
        beforeEach(() => {
            // TODO: Clean up this mess!!! Remove hacky workaround for adding padding
            // to leaflets fitBounds methods from sidebar-width
            mapDiv = document.createElement("div");
            sidebarDiv = document.createElement("div");
            sidebarDiv.id = "hotspot-sidebar";
            document.body.appendChild(sidebarDiv);
            document.body.appendChild(mapDiv);
            store.LMap = L.map(mapDiv);
            store.attribute = "a_attr";
            store.datasetMeta = {
                crs: { init: "epsg:1234" },
                crs_wkt: "longdefs",
            };
            // Mocking L.Proj does not work somehow so we circumvent this issue by
            // just returning a normal L.GeoJSON
            L.Proj = {
                geoJson: jest.fn(
                    (dataset, options) => new L.GeoJSON(dataset, options)
                ),
            };
        });
        afterEach(() => {
            document.body.innerHTML = "";
            store.LMap.remove();
            store.LMap = undefined;
            store.attribute = undefined;
            store.datasetMeta = undefined;
        });
        test("calls proj4.defs with crs and crs_wkt from dataset meta", () => {
            const wrapper = mount(HHotspotContainer, {
                attachToDocument: true,
            });
            wrapper.vm.onAnalyzed(analyzedDataset);
            expect(proj4.defs).toHaveBeenCalledWith(
                store.datasetMeta.crs.init,
                store.datasetMeta.crs_wkt
            );
        });
        test("calls geojsonToMap with epsg parameter", () => {
            const spy = jest.spyOn(maputils, "geojsonToMap");
            const wrapper = mount(HHotspotContainer, {
                attachToDocument: true,
            });
            wrapper.vm.onAnalyzed(analyzedDataset);
            expect(spy).toHaveBeenCalledWith(
                expect.any(Object),
                store.LMap,
                {
                    style: expect.any(Function),
                    name: "analyzed_dataset",
                    pane: "splitmap_left",
                },
                {
                    paddingBottomRight: expect.any(Array),
                },
                store.datasetMeta.crs.init
            );
        });
        test("adds the analyzed dataset to the map", () => {
            const wrapper = mount(HHotspotContainer, {
                attachToDocument: true,
            });
            wrapper.vm.onAnalyzed(analyzedDataset);
            const layerNames = [];
            store.LMap.eachLayer(lyr => {
                layerNames.push(lyr.options.name);
            });
            expect(layerNames).toContain("analyzed_dataset");
        });
        test("adds the analyzed dataset to pane 'splitmap_left'", () => {
            const wrapper = mount(HHotspotContainer, {
                attachToDocument: true,
            });
            wrapper.vm.onAnalyzed(analyzedDataset);
            const layerNames = [];
            store.LMap.eachLayer(lyr => {
                layerNames.push(lyr.options.pane);
            });
            expect(layerNames).toContain("splitmap_left");
        });
        test("adds a LLegend component for the analyzed dataset", () => {
            const wrapper = mount(HHotspotContainer, {
                attachToDocument: true,
            });
            wrapper.vm.onAnalyzed(analyzedDataset);
            expect(wrapper.vm.legend.$options.name).toEqual("LLegend");
        });
    });
});
