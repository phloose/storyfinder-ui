import $ from "jquery";
import Vue from "vue";
import { mount } from "@vue/test-utils";

import HLoadContainer from "storyfinder/hotspot/components/containers/HLoadContainer.vue";
import EventBus from "storyfinder/EventBus";
import store from "storyfinder/store";

const dataset = {
    type: "FeatureCollection",
    features: [
        {
            type: "Feature",
            properties: {
                a_attr: 1.0,
                b_attr: 4.0,
                c_attr: 7.0,
                d: 10,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
        {
            type: "Feature",
            properties: {
                a_attr: 2.0,
                b_attr: 5.0,
                c_attr: 8.0,
                d: 11,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
        {
            type: "Feature",
            properties: {
                a_attr: 3.0,
                b_attr: 6.0,
                c_attr: 9.0,
                d: 12,
            },
            geometry: { type: "Point", coordinates: [0, 0] },
        },
    ],
};
const datasetMeta = {
    crs: { init: "epsg:1234" },
    crs_wkt: "longdefs",
};
const datasetPath = "this/is/a/path";

describe("HLoadContainer", () => {
    let dfd;
    let loaded;
    const inputEvent = {
        target: { files: [{ path: datasetPath }] },
    };
    beforeEach(() => {
        dfd = $.Deferred();
        loaded = { meta: datasetMeta, dataset };
    });
    afterEach(() => {
        jest.resetAllMocks();
        jest.restoreAllMocks();
        loaded = undefined;
    });
    test("renders correctly", () => {
        const wrapper = mount(HLoadContainer);
        expect(wrapper.html()).toMatchSnapshot();
    });
    test("shows a hint in the attribute dropdown until a dataset has been loaded", async () => {
        const wrapper = mount(HLoadContainer);
        const dropdown = wrapper.find("select").findAll("option");
        expect(dropdown.at(0).text()).toContain("Load a dataset");
        $.getJSON.mockImplementation(() => dfd.resolve(loaded));
        wrapper.vm.loadDataset(inputEvent);
        await Vue.nextTick();
        expect(dropdown.at(0).text()).toContain("Select an attribute");
        expect(wrapper.html()).toMatchSnapshot();
    });
    test("calls onDatasetLoaded when loading dataset has finished", () => {
        const spy = jest.spyOn(HLoadContainer.methods, "onDatasetLoaded");
        const wrapper = mount(HLoadContainer);
        $.getJSON.mockImplementation(() => dfd.resolve(loaded));
        wrapper.vm.loadDataset(inputEvent);
        expect(spy).toHaveBeenCalledWith({
            dataset,
            datasetMeta,
            datasetPath,
        });
    });
    test("calls setAttribute when change event is emitted in attribute dropdown", () => {
        const spy = jest.spyOn(HLoadContainer.methods, "setAttribute");
        const wrapper = mount(HLoadContainer);
        const dropdown = wrapper.find("select");
        dropdown.trigger("change");
        expect(spy).toHaveBeenCalled();
    });
    test("calls resetSelection when focus event is emitted in attribute dropdown", () => {
        const spy = jest.spyOn(HLoadContainer.methods, "resetSelection");
        const wrapper = mount(HLoadContainer);
        const dropdown = wrapper.find("select");
        dropdown.trigger("focus");
        expect(spy).toHaveBeenCalled();
    });
    describe("loaderAnimation", () => {
        test("is shown during request", () => {
            const wrapper = mount(HLoadContainer);
            const spy = jest.spyOn(wrapper.vm.container, "loaderAnimation");
            $.getJSON.mockImplementation(() => {
                expect(spy).toHaveBeenNthCalledWith(1, true);
                return dfd.resolve(loaded);
            });
            wrapper.vm.loadDataset(inputEvent);
        });
        test("stops on success", () => {
            const wrapper = mount(HLoadContainer);
            const spy = jest.spyOn(wrapper.vm.container, "loaderAnimation");
            $.getJSON.mockImplementation(() => dfd.resolve(loaded));
            wrapper.vm.loadDataset(inputEvent);
            expect(spy).toHaveBeenLastCalledWith(false);
        });
        test("stops on failure", () => {
            const wrapper = mount(HLoadContainer);
            const spy = jest.spyOn(wrapper.vm.container, "loaderAnimation");
            $.getJSON.mockImplementation(() => dfd.reject());
            wrapper.vm.loadDataset(inputEvent);
            expect(spy).toHaveBeenLastCalledWith(false);
        });
    });
    describe("methods", () => {
        describe("loadDataset", () => {
            test("on-success: emits 'dataset-loaded' event to EventBus", () => {
                const spy = jest.spyOn(EventBus, "$emit");
                const wrapper = mount(HLoadContainer);
                $.getJSON.mockImplementation(() => dfd.resolve(loaded));
                wrapper.vm.loadDataset(inputEvent);
                expect(spy).toHaveBeenCalledWith("dataset-loaded", {
                    dataset,
                    datasetMeta,
                    datasetPath,
                });
            });
            test("on-error: calls container showError method", () => {
                const wrapper = mount(HLoadContainer);
                const spy = jest.spyOn(wrapper.vm.container, "showError");
                $.getJSON.mockImplementation(() => dfd.reject());
                wrapper.vm.loadDataset(inputEvent);
                expect(spy).toHaveBeenCalled();
            });
        });
        describe("setAttribute", () => {
            const setAttributeEvent = {
                target: { value: "someValue" },
            };
            test("sets store.attribute to event value", () => {
                const wrapper = mount(HLoadContainer);
                wrapper.vm.setAttribute(setAttributeEvent);
                expect(store.attribute).toEqual("someValue");
            });
            test("emits event 'attribute-selected' to EventBus", () => {
                const spy = jest.spyOn(EventBus, "$emit");
                const wrapper = mount(HLoadContainer);
                wrapper.vm.setAttribute(setAttributeEvent);
                expect(spy).toHaveBeenCalledWith("attribute-selected");
            });
        });
    });
});
