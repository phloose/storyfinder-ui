import Vue from "vue";
import $ from "jquery";
import { mount } from "@vue/test-utils";

import HMoranContainer from "storyfinder/hotspot/components/containers/HMoranContainer.vue";
import store from "storyfinder/store";

describe("HMoranContainer", () => {
    let wrapper;
    let dfd;
    beforeEach(() => {
        wrapper = mount(HMoranContainer);
        // loadMoran does nothing if there is no dataset
        // so we give it a truthy value
        store.dataset = "dataset";
        dfd = $.Deferred();
    });
    afterEach(() => {
        wrapper.destroy();
        jest.restoreAllMocks();
        store.dataset = undefined;
        $.getJSON.mockClear();
    });
    test("renders correctly", () => {
        expect(wrapper.html()).toMatchSnapshot();
    });
    test("loadMoran (success): calls result containers setMoranResult method", () => {
        const spy = jest
            .spyOn(wrapper.vm.$refs.result, "setMoranResult")
            .mockImplementation(() => {});
        $.getJSON.mockImplementation(() =>
            dfd.resolve({
                moransI: 0.5,
                moransP: 0.07,
            })
        );
        wrapper.find("#moran-button").trigger("click");
        expect(spy).toHaveBeenCalledWith({ moransI: 0.5, moransP: 0.07 });
    });
    test("loadMoran (failure): calls containers showError method", () => {
        const spy = jest.spyOn(wrapper.vm.container, "showError");
        $.getJSON.mockImplementation(() => dfd.reject());
        wrapper.find("#moran-button").trigger("click");
        expect(spy).toHaveBeenCalled();
    });
    describe("results", () => {
        test("shows correct text for clustered result", async () => {
            $.getJSON.mockImplementation(() =>
                dfd.resolve({ moransI: 0.5, moransP: 0.04 })
            );
            wrapper.find("#moran-button").trigger("click");
            await Vue.nextTick();
            expect(wrapper.text()).toContain(
                "The selected attribute appears to be spatially clustered"
            );
            expect(wrapper.html()).toMatchSnapshot();
        });
        test("shows correct text for random result", async () => {
            $.getJSON.mockImplementation(() =>
                dfd.resolve({ moransI: 0.5, moransP: 0.05 })
            );
            wrapper.find("#moran-button").trigger("click");
            await Vue.nextTick();
            expect(wrapper.text()).toContain(
                "The spatial distribution of the selected attribute is not statistically significant"
            );
            expect(wrapper.html()).toMatchSnapshot();
        });
        test("shows correct text for dispersed result", async () => {
            $.getJSON.mockImplementation(() =>
                dfd.resolve({ moransI: -0.5, moransP: 0.04 })
            );
            wrapper.find("#moran-button").trigger("click");
            await Vue.nextTick();
            expect(wrapper.text()).toContain(
                "The selected attribute appears to be spatially evenly distributed"
            );
            expect(wrapper.html()).toMatchSnapshot();
        });
    });
});
