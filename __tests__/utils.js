import { shallowMount, mount } from "@vue/test-utils";

export function getMountedComponent(Component, propsData, shallow = true) {
    const fn = shallow ? shallowMount : mount;
    return fn(Component, {
        propsData,
    });
}

export default getMountedComponent;
