module.exports = {
    testPathIgnorePatterns: [
        "__tests__/integration/renderer/storyfinder/jest.setup.js",
        "__tests__/.eslintrc.js",
        "__tests__/utils.js",
    ],
    modulePathIgnorePatterns: ["node_modules"],
    moduleFileExtensions: ["js", "vue"],
    transform: {
        "^.+\\.(js|jsx|ts|tsx)$": "babel-jest",
        "^.+\\.(css|styl|less|sass|scss)$": "jest-css-modules-transform",
        "^.+\\.(md)$": "markdown-loader-jest",
        "^.+\\.(vue)$": "vue-jest",
    },
    moduleNameMapper: {
        "^.+\\.(css|styl|less|sass|scss)$": "identity-obj-proxy",
        "^storyfinder(.*)$": "<rootDir>/src/renderer/storyfinder/$1",
        "^hotspot(.*)$": "<rootDir>/src/renderer/storyfinder/hotspot/$1",
        "^utils(.*)$": "<rootDir>/src/renderer/storyfinder/utils.js",
        "^maputils(.*)$": "<rootDir>/src/renderer/storyfinder/maputils.js",
    },
    collectCoverage: true,
    collectCoverageFrom: ["src/renderer/**/*.{js,vue}"],
    snapshotResolver: "./snapshotResolver.js",
};
