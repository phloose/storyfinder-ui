// Modules to control application life and create native browser window
const { app, BrowserWindow, dialog, shell } = require("electron");
const path = require("path");
const subprocess = require("child_process");

function spawnPyBackend(options) {
    const pythonBin = process.platform === "win32" ? "python" : "bin/python";
    const pythonPath = app.isPackaged
        ? `py_interpreter/${pythonBin}`
        : `src/python/py_interpreter/${pythonBin}`;
    const serverPath = app.isPackaged
        ? "server/server.py"
        : "src/python/server.py";
    options.env = app.isPackaged
        ? options.env
        : { STORYFINDER_DEV: 1, ...options.env };
    return subprocess.spawn(pythonPath, [serverPath], options);
}

function logPyBackend(win, channel, msg) {
    console.log(msg);
    win.webContents.send(channel, msg);
}

function createWindow() {
    // Create the browser window.
    const mainWindow = new BrowserWindow({
        width: 1400,
        height: 900,
        minWidth: 1400,
        minHeight: 900,
        webPreferences: {
            preload: path.join(__dirname, "preload.js"),
            nodeIntegration: true,
            webSecurity: !!app.isPackaged,
        },
    });

    mainWindow.removeMenu();

    // Spawn python backend.
    const pyBackend = spawnPyBackend({
        cwd: app.isPackaged ? path.dirname(app.getAppPath()) : ".",
        env: process.env,
    });

    // Log stdout and stderr emitted by the python backend to the renderer console for
    // enhanced debugging

    // We need to wait until the browser window has finished loading to receive the
    // emitted messages
    mainWindow.webContents.on("did-finish-load", () => {
        pyBackend.stdout.on("data", data => {
            logPyBackend(
                mainWindow,
                "pyBackend:stdout",
                `pybackend:stdout:\n${data.toString("ascii")}`
            );
        });

        pyBackend.stderr.on("data", data => {
            logPyBackend(
                mainWindow,
                "pyBackend:stderr",
                `pybackend:stderr:\n${data.toString("ascii")}`
            );
        });
    });

    pyBackend.on("error", err => {
        dialog.showMessageBox({
            type: "error",
            message: `Error while spawning python backend: \n\n${err}`,
            buttons: ["OK"],
        });
    });

    // Redirect new windows (target="_blank") to the OS's default browser
    mainWindow.webContents.on("new-window", (event, url) => {
        event.preventDefault();
        shell.openExternal(url);
    });

    // and load the index.html of the app.
    if (app.isPackaged) {
        mainWindow.loadFile("index.html");
    } else {
        mainWindow.loadURL(
            `http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}`
        );
    }
    // Open the DevTools.
    if (!app.isPackaged) {
        mainWindow.webContents.openDevTools();
    }

    // Maximize window after initialization
    // mainWindow.maximize();

    mainWindow.on("close", function () {
        // https://stackoverflow.com/questions/32705857/cant-kill-child-process-on-windows/32814686
        if (process.platform === "win32") {
            subprocess.exec(`taskkill /pid ${pyBackend.pid} /T /F`);
        } else {
            pyBackend.kill("SIGKILL");
        }
    });
    if (process.env.NODE_ENV !== "production") {
        // eslint-disable-next-line global-require
        require("vue-devtools").install();
    }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", function () {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== "darwin") app.quit();
});

app.on("activate", function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
