const { remote } = require("electron");
const vega = require("vega");
const vegaLite = require("vega-lite");
const vegaLiteAPI = require("vega-lite-api");
const vegaEmbed = require("vega-embed");
const { ipcRenderer } = require("electron");

window.remote = remote;
window.vegaAPI = {
    vega,
    vegaLite,
    vegaLiteAPI,
    vegaEmbed,
};
window.ipcRenderer = ipcRenderer;
