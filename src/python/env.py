"""Setup local environment for shared libraries"""

import os
import sys

if "resources" in os.getcwd(): # app is packaged
    env_path = os.path.join(os.getcwd(), "py_interpreter")
else:
    env_path = os.path.join(os.getcwd(), "src", "python", "py_interpreter")

orig_path = os.environ["PATH"]

if sys.platform == "win32":
    path = os.pathsep.join([
        env_path,
        os.path.join(env_path, "Scripts"),
        os.path.join(env_path, "DLLs"),
        os.path.join(env_path, "bin"),
        os.path.join(env_path, "Library", "bin"),
        os.path.join(env_path, "Library", "usr", "bin"),
        os.path.join(env_path, "Library", "mingw-w64","bin"),
        orig_path
    ])
    os.environ["USE_PATH_FOR_GDAL_PYTHON"] = "YES"
elif sys.platform in ("linux", "darwin"):
    path = os.pathsep.join([
        os.path.join(env_path, "DLLs"),
        os.path.join(env_path, "lib"),
        orig_path
    ])

os.environ["PATH"] = path
