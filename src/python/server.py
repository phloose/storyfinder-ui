# flake8: noqa

import os
import env
from flask import Flask, request, jsonify
from storyfinder.analyze.hotspot import HotSpot
from storyfinder.analyze.utils import morans_i
from storyfinder.io import VectorDataset

from utils import to_geojson

app = Flask(__name__)

DEBUG = False if os.getenv("STORYFINDER_DEV") is None else True

# Definition of endpoints used in StoryFinder-UI
#
# For ease of use only return data as JSON via "flask.jsonify" function.


@app.route("/hotspot")
def hotspot():
    path = request.args.get("path")
    attr = request.args.get("attr")
    dataset = VectorDataset(path)
    hs = HotSpot(dataset, attr, wtype="q")
    hs.calc()
    return jsonify(hs.result.as_geojson())


@app.route("/load")
def load():
    path = request.args.get("path")
    dataset = VectorDataset(path)
    return jsonify({"meta": dataset.meta, "dataset": to_geojson(dataset)})


@app.route("/moran")
def morans():
    dataset = request.args.get("dataset")
    attribute = request.args.get("attribute")
    m = morans_i(VectorDataset(dataset), attribute, wtype="q")
    return jsonify({"moransI": m.I, "moransP": m.p_norm})


if __name__ == "__main__":
    app.run(port=4242, debug=DEBUG)
