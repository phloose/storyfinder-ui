import asyncio
import sys
import os

tmp_map = {"win32": "%temp%", "linux": "/tmp", "darwin": "/tmp"}

tmp = tmp_map[sys.platform]

if os.path.join("python", "py_interpreter") not in os.getcwd():  # project root
    env_path = os.path.join(os.getcwd(), "src", "python", "py_interpreter")
else:  # inside py_interpreter folder
    os.chdir("../")
    env_path = os.path.join(os.getcwd(), "py_interpreter")

python_bin = os.path.join("bin", "python") if not sys.platform == "win32" else "python"

cmds = [
    f"curl https://gitlab.com/phloose/storyfinder/-/raw/master/env.yml > {tmp}"
    "/storyfinder-env.yml",
    f"conda env update -p {env_path} --file {tmp}/storyfinder-env.yml",
    f"{os.path.join(env_path, python_bin)} -m pip install --upgrade git+https://gitlab.com/phloose/storyfinder.git",
]


# from: https://stackoverflow.com/questions/59259565/asyncio-stream-subprocess-output-and-pdb
async def print_stream(stream):
    while True:
        line = await stream.readline()
        if line:
            print(line.decode("utf-8"), end="")
        else:
            break


# mostly from: https://docs.python.org/3/library/asyncio-subprocess.html
async def exec_cmd(cmd):
    print(f"Executing command: '{cmd}'\n")

    process = await asyncio.create_subprocess_shell(
        cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE,
    )
    await asyncio.wait([print_stream(process.stdout), print_stream(process.stderr)])
    await process.wait()

    if process.returncode != 0:
        sys.exit(-1)


for cmd in cmds:
    asyncio.run(exec_cmd(cmd))

sys.exit(0)
