from storyfinder.io import VectorDataset


def to_geojson(dataset: VectorDataset):
    """Return a GeoJSON like mapping of a VectorDataset

    Args:
        dataset (VectorDataset): The dataset to convert

    Returns:
        dict: GeoJSON like mapping of a VectorDataset
    """
    return {
        "type": "FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "properties": feature["properties"],
                "geometry": feature["geometry"],
            }
            for feature in dataset.rows
        ],
    }
