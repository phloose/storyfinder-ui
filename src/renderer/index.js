/* Entry point of renderer process */

import { ipcRenderer } from "electron";

/**
 * Webpack specific imports
 *
 * Include all needed css-files, packages and src-modules in the order they depend on
 * each other.
 */
import "font-awesome/css/font-awesome.css";
import "fomantic-ui-css/semantic.css";
import "fomantic-ui-css/semantic";

import "leaflet/dist/leaflet.css";
import "sidebar-v2/css/leaflet-sidebar.css";
import "leaflet-splitmap/src/layout.css";
import "leaflet-splitmap/src/range.css";
import "./custom.css";

import "leaflet";
import "sidebar-v2/js/leaflet-sidebar";
import "leaflet-splitmap";
import "proj4";
import "proj4leaflet";

import Vue from "vue";
import VueNonreactive from "vue-nonreactive";

// StoryFinder modules
import HotspotMain from "storyfinder/hotspot/Main.vue";

// Hot Module Reload should reload the whole page because of state management
if (module.hot) {
    module.hot.dispose(() => window.location.reload());
}

// Receive stdout and stderr emitted by the python backend from the main process
ipcRenderer.on("pyBackend:stdout", (event, message) => {
    console.log(message);
});
ipcRenderer.on("pyBackend:stderr", (event, message) => {
    console.error(message);
});

Vue.use(VueNonreactive);

// eslint-disable-next-line no-unused-vars
const StoryFinder = new Vue({
    el: "#storyfinder",
    render(h) {
        return h(HotspotMain);
    },
});
