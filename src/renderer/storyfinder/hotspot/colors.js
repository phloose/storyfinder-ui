import * as chroma from "chroma-js";

export const blueWhiteRed = chroma
    .scale(["blue", "white", "red"])
    .classes([-3.39, -2.58, -1.96, -1.65, 1.65, 1.96, 2.58, 3.39]);

export default {
    blueWhiteRed,
};
