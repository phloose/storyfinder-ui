### Hotspot Analysis

The hotspot analysis identifies statistically significant *local* patterns in a dataset
with respect to a specified attribute. For a single spatial entity the attribute values
of its neighbors and the entire dataset are considered. A **hotspot**/**coldspot**  is
then a spatial entity or an area with a significantly higher/lower attribute value than
its surrounding.

This analysis method is also referenced as **Getis-Ord** or **G** statistic. The result of
this statistic is the **z-value** or **z-score**. It indicates the statistical significance of
a local observation. Values between *-1.65* and *1.65* are not significant, whereas values
below *-1.65* resp. above *1.65* indicate statistically significant observations in the
negative resp. positive direction compared to its surrounding.

Futher information can be found in:

Getis, A. and Ord, J.K. (1992), The Analysis of Spatial Association by Use of Distance Statistics. Geographical Analysis, 24: 189-206. <a href="https://doi.org/10.1111/j.1538-4632.1992.tb00261.x" target="_blank">doi:10.1111/j.1538-4632.1992.tb00261.x</a>

Ord, J.K. and Getis, A. (1995), Local Spatial Autocorrelation Statistics: Distributional Issues and an Application. Geographical Analysis, 27: 286-306. <a href="https://doi.org/10.1111/j.1538-4632.1995.tb00912.x" target="_blank">doi:10.1111/j.1538-4632.1995.tb00912.x</a>

<a href="https://desktop.arcgis.com/de/arcmap/10.3/tools/spatial-statistics-toolbox/h-how-hot-spot-analysis-getis-ord-gi-spatial-stati.htm" target="_blank">Hot-Spot-Analysis (ArcGIS help)</a>
