### Cluster tendency

For measuring the cluster tendency (also called the degree of spatial autocorrelation)
the **Moran's Index** (MI) can be used.
It is a statistic which is applied to an entire dataset to evaluate if its spatial
entities with respect to a specified attribute tend to be **clustered**,
**randomly distributed** or **dispersed**. It is a good measure to see if a hotspot
analysis will bring any further insights.

The values of MI range from -1 to 1, where the former indicates perfect dispersion and the
latter perfect clustering, in other words the degree of clustering or dispersion.
It must always be interpreted with respect to statistical significance, i.e.
the calculated Index is not the result of random spatial processes.

Further information can be found in:

A.D. Cliff and J.K. Ord. Spatial Processes: Models and Applications. Pion, London, 1981.

Moran, P. (1948). The Interpretation of Statistical Maps. Journal of the Royal Statistical Society. Series B (Methodological), 10(2), 243-251.
<a href="https://www.jstor.org/stable/2983777" target="_blank">www.jstor.org/stable/2983777</a>

Moran, P. (1950). Notes on Continuous Stochastic Phenomena. Biometrika, 37(1/2), 17-23. <a href="https://doi.org/10.2307/2332142" target="_blank">doi:10.2307/2332142</a>

<a href="https://pro.arcgis.com/de/pro-app/tool-reference/spatial-statistics/h-how-spatial-autocorrelation-moran-s-i-spatial-st.htm" target="_blank">Spatial Autocorrelation (ArcGIS help)</a>


