### Numerical classification

#### Ckmeans (Jenks)

*Ckmeans* clustering splits the given attribute's data range into optimal clusters based
on the similarity of the data points. Thereby the differences in each cluster are
minimized. It is an improvement on heuristic-based approaches like *Jenks Natural Breaks*.

Reference: [Ckmeans.1d.dp: Optimal k-means Clustering in One Dimension by Dynamic Programming Haizhou Wang and Mingzhou Song ISSN 2073-4859](https://journal.r-project.org/archive/2011-2/RJournal_2011-2_Wang+Song.pdf)

#### Quantiles

Using *Quantiles* classification every class has the same amount of data points.

#### Equal Interval

*Equal Interval* classification splits the given attribute's data range into equal sized
data intervals.
