### Hotspot Analysis

Identify and visualize local clusters (groups of remarkably <span
style="color:red">high</span> resp. <span style="color:blue">low</span> values) for the
selected attribute.
