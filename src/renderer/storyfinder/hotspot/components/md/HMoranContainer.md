### Cluster tendency

Check whether the selected attribute shows a tendency to be spatially clustered or
appears spatially evenly distributed (see image below). If there is a low cluster
tendency  a hotspot analysis will not bring further insights.
