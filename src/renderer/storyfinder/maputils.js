/**
 * Various helper functions for map related tasks
 * @module maputils
 */

import L from "leaflet";

/**
 * Remove the geojson with the given name from the map
 *
 * @param {L.Map} map The map instance
 * @param {string} name the name of the geojson layer
 */
export function removeGeoJSON(map, name) {
    map.eachLayer(lyr => {
        if (name === lyr.options.name) {
            map.removeLayer(lyr);
        }
    });
}

/**
 * Add a leaflet geoJSON layer to a leaflet map instance
 *
 * @param {JSON} dataset A geojson representing a dataset
 * @param {L.Map} map A leaflet map instance
 * @param {Object} options Options to pass the geojson constructor
 * @param {Object} paddingOptions Options passed to flyToBounds/fitBounds
 * @param {string} epsg When set use L.Proj.geoJson. Inject crs info into dataset
 * @param {boolean} [fly=true] Whether to animate the bounds fitting.
 * @returns {L.GeoJSON}
 */
export function geojsonToMap(
    dataset,
    map,
    options,
    paddingOptions,
    epsg,
    fly = true
) {
    let geojson;
    if (epsg) {
        dataset.crs = {
            type: "name",
            properties: {
                name: epsg,
            },
        };
        geojson = L.Proj.geoJson(dataset, options).addTo(map);
    } else {
        geojson = L.geoJSON(dataset, options).addTo(map);
    }
    if (fly) {
        map.flyToBounds(geojson.getBounds(), paddingOptions || {});
    } else {
        map.fitBounds(geojson.getBounds(), paddingOptions || {});
    }
    return geojson;
}

/**
 * Removes all elements that belong to a splitMap from the map.
 *
 * Can use a callback that is invoked after all elements have been removed.
 *
 * @param {L.control.SplitMap} splitMap The splitMap instance
 * @param {L.Map} map The map instance
 * @param {L.Control} legend the control/legend instance
 * @param {Function} callback A callback that is invoked when splitMap is removed
 */
export function removeSplitMap(splitMap, map, legend, callback) {
    splitMap.remove();
    removeGeoJSON(map, "compare_dataset");
    legend.remove();
    if (callback && typeof callback === "function") {
        callback();
    }
}

/**
 * Create a legend on a map. To be used with an instance of L.control and the onAdd
 * property
 *
 * @param {string} title The title of the legend.
 * @param {chroma.Scale} chromaColorScale A chroma colorscale.
 * @param {Number[]} breaks An array containing the class breaks for the legend items.
 * @param {number} [precision=2] The precision for floating point numbers
 * @returns {HTMLDivElement} A function that returns a div element containing the legend
 */
export function createLegend(title, chromaColorScale, breaks, precision = 2) {
    const colorScale = chromaColorScale.classes(breaks);
    return function () {
        const div = L.DomUtil.create("div", "info legend");
        // As the breaks array starts from low numbers we need to reverse it here
        // because we want the legend to show higher numbers at the top.
        const breaksReversed = breaks.slice().reverse();
        div.innerHTML = `<b>${title}</b><br>`;
        for (let i = 0; i < breaksReversed.length; i++) {
            // Since we want to display the value range (e.g class) that a color stands
            // for we need to extract the upper and lower boundary of the breaks array.
            // We do that by using arr[i] and arr[i + 1]  until the end of the array is
            // reached (lower is undefined) and break out.
            const upper = breaksReversed[i];
            const lower = breaksReversed[i + 1];
            if (typeof lower === "undefined") break;
            div.innerHTML += `<i style="background:${colorScale(
                (lower + upper) / 2
            ).hex()}"></i>${
                Number.isInteger(lower) ? lower : lower.toFixed(precision)
            } - ${
                Number.isInteger(upper) ? upper : upper.toFixed(precision)
            }<br>`;
        }
        return div;
    };
}

/**
 * Highlight features of a leaflet Layer instance of which a specified attribute falls in
 * the specified range given by lower and upper
 *
 * highlightOpt and normalOpt need to be objects that specify Leaflet path options.
 * See https://leafletjs.com/reference-1.6.0.html#path-option
 *
 * @param {L.Layer} dataset A Leaflet Layer instance
 * @param {string} attr The attribute for which the feature should be highlighted
 * @param {number} lower The lower boundary
 * @param {number} upper The upper boundary
 * @param {Object} highlightOpt Style for highlighting
 * @param {Object} normalOpt Style for normal appearance
 */
export function highlightFeatures(
    dataset,
    attr,
    lower,
    upper,
    highlightOpt,
    normalOpt
) {
    const highLight = highlightOpt || {
        weight: 3.5,
        color: "#00ff00",
    };
    const normal = normalOpt || {
        weight: 1.5,
        color: "grey",
    };
    dataset.getLayers().forEach(lyr => {
        if (
            lower < lyr.feature.properties[attr] &&
            lyr.feature.properties[attr] < upper
        ) {
            lyr.setStyle(highLight);
            lyr.bringToFront();
        } else {
            lyr.setStyle(normal);
        }
    });
}

/**
 * Reset selected features of a Leaflet Layer instance
 *
 * Used as callback when double clicking a vega chart.
 *
 * If used defaulStyle needs to be an object that specifies Leaflet Path options.
 * See https://leafletjs.com/reference-1.6.0.html#path-option
 *
 * @param {L.Layer} dataset A Leaflet Layer instance.
 * @param {Object} defaultStyle The default style.
 * @returns {Function} The actual callback.
 */
export function resetSelectedFeatures(dataset, defaultStyle) {
    const style = defaultStyle || {
        color: "grey",
        weight: 1.5,
    };
    return function () {
        dataset.getLayers().forEach(lyr => {
            lyr.setStyle(style);
        });
    };
}
