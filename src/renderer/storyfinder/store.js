/* eslint-disable max-classes-per-file */
/* eslint-disable no-underscore-dangle */
import EventBus from "storyfinder/EventBus";

class LegendStore {
    constructor() {
        this.legends = {};
    }

    getLegend(name) {
        try {
            return this.legends[name];
        } catch (error) {
            throw new Error(`Legend with name ${name} does not exist.`);
        }
    }

    setLegend(name, legend) {
        this.legends[name] = legend;
    }

    deleteLegend(name) {
        delete this.legends[name];
    }
}
const legendStore = new LegendStore();

class Store {
    get LMap() {
        return this._lmap;
    }

    set LMap(value) {
        this._lmap = value;
    }

    get LSidebar() {
        return this._lsidebar;
    }

    set LSidebar(value) {
        this._lsidebar = value;
    }

    get LTileLayer() {
        return this._ltilelayer;
    }

    set LTileLayer(value) {
        this._ltilelayer = value;
    }

    get LLegends() {
        return legendStore;
    }

    get LSplitMap() {
        return this._lsplitmap;
    }

    set LSplitMap(value) {
        this._lsplitmap = value;
    }

    get dataset() {
        return this._dataset;
    }

    set dataset(value) {
        this._dataset = value;
    }

    get datasetMeta() {
        return this._datasetMeta;
    }

    set datasetMeta(value) {
        this._datasetMeta = value;
    }

    get attribute() {
        return this._attribute;
    }

    set attribute(value) {
        this._attribute = value;
    }

    get analyzedDataset() {
        return this._analyzedDataset;
    }

    set analyzedDataset(value) {
        this._analyzedDataset = value;
    }

    get analyzedAttribute() {
        return this._analyzedAttribute;
    }

    set analyzedAttribute(value) {
        this._analyzedAttribute = value;
    }

    get datasetPath() {
        return this._datasetPath;
    }

    set datasetPath(value) {
        this._datasetPath = value;
    }

    get mapDataset() {
        return this._mapdataset;
    }

    set mapDataset(value) {
        this._mapdataset = value;
        EventBus.$emit("mapdataset-set", value);
    }
}
export default new Store();
