/**
 * Various helper functions for trivial and non-trivial tasks
 * @module utils
 */
import path from "path";
import * as url from "url";

import * as chroma from "chroma-js";
import * as ss from "simple-statistics";
import * as vla from "vega-lite-api";

/**
 * Convert a string to title case
 *
 * @param {string} str String to be converted to title case.
 * @returns {string} The title cased string.
 */
export function toTitleCase(str) {
    return str.charAt(0).toUpperCase() + str.substr(1);
}

/**
 * Clear the options of a select element
 *
 * @param {HTMLSelectElement} dropdown The dropdown element that is to be cleared.
 * @param {boolean} skipfirst Whether to skip the first element. Often a placeholder
 */
export function clearOptions(dropdown, skipfirst = true) {
    const optcnt = dropdown.options.length;
    for (let i = optcnt; i >= skipfirst ? 1 : 0; i--) {
        dropdown.remove(i);
    }
}

/**
 * Get the attributes of a dataset and append it to the given dropdown selection
 *
 * Only numeric attributes are added to the dropdown selection.
 *
 * @param {JSON} dataset A GeoJSON like mapping representing a dataset.
 * @param {HTMLSelectElement} dropdown The select element to append the options-
 */
export function populateOptions(dataset, dropdown) {
    const props = dataset.features[0].properties;
    Object.entries(props).forEach(prop => {
        const [attr, value] = prop;
        if (typeof value === "number" && !Number.isNaN(value)) {
            const opt = document.createElement("option");
            opt.value = attr;
            opt.text = attr;
            dropdown.add(opt);
        }
    });
}

/**
 * Get the property objects of a dataset and return them as an array of objects
 *
 * @param {JSON} dataset A dataset with a GeoJSON like mapping.
 * @returns {Object[]} An array of objects holding the properties of datasets features.
 */
export function getProperties(dataset) {
    const props = [];
    for (let i = 0; i < dataset.features.length; i++) {
        props.push(dataset.features[i].properties);
    }
    return props;
}

/**
 * Return classification for given Moran's I and corresponding p-value.
 *
 * @param {number} moransI Moran's I.
 * @param {number} moransP P-value for Moran's I.
 * @returns {string} The result of the classification ("clustered", "dispersed", "random").
 */
export function classifyMoran(moransI, moransP) {
    const significant = moransP < 0.05;
    if (moransI > 0 && significant) {
        return "clustered";
    }
    if (moransI < 0 && significant) {
        return "dispersed";
    }
    return "random";
}

/**
 * Split an array of data points into N classes with a specific classification method
 *
 * @param {number[]} data An array of data points.
 * @param {number} clsNum Number of desired classes.
 * @param {("ckmeans"|"jenks"|"quantiles"|"equal")} method The classification method.
 * @returns {number[]} An array with the class breaks.
 */
export function classify(data, clsNum, method) {
    switch (method) {
        case "ckmeans":
        case "jenks": {
            // According to simple-statistics library ckmeans does the same as jenks natural
            // breaks.
            // It seems a bit hacky to me to only return the first element of the cluster as
            // adviced in the docs. We take it for now until there is a better solution.
            // Reference: https://github.com/simple-statistics/simple-statistics/blob/727eaed049af4f788fb2299e5c8263573618e78c/CHANGELOG.md#jenks---ckmeans
            // Fix for #34
            const clusters = ss.ckmeans(data, clsNum + 1);
            return clusters.map((cluster, idx) =>
                idx + 1 === clusters.length ? Math.max(...cluster) : cluster[0]
            );
        }
        case "quantiles":
            return chroma.limits(data, "q", clsNum);
        case "equal":
            return chroma.limits(data, "e", clsNum);
        default:
            return [];
    }
}

/**
 * Creates vega-lite specifications for a histogram with range selection for given data
 * and attribute.
 *
 * @param {Object[]} data An array of objects holding the data as properties.
 * @param {string} attr The attribute for which the histogram is to be created.
 * @returns {JSON} The vega-lite specification for the created histogram.
 */
export function vegaHistogram(data, attr) {
    // TODO: Find out if it is possible to add a selection after vegaSpecs has been
    // constructed.
    const vegaSpecs = vla
        .markBar()
        .data({ values: data })
        .encode(vla.x().fieldQ(attr).bin(true), vla.y().count())
        .width("container")
        .select(vla.selectInterval("range").encodings("x"));

    return vegaSpecs.toSpec();
}

/**
 * Create a paragraph element that appends elements from the given array as children
 *
 * @param {HTMLElement[]} elements An array of html elements.
 * @returns {HTMLParagraphElement} Paragraph element holding child elements.
 */
export function addParagraphWith(elements) {
    const p = document.createElement("p");
    if (!elements || !Array.isArray(elements)) return p;
    elements.forEach(el => {
        p.appendChild(el);
    });
    return p;
}

/**
 * Creates a HTML element with a corresponding label
 *
 * @param {HTMLElement} elType The type of the html element.
 * @param {string} elId The id for the html element.
 * @param {string} labelText The text to be displayed as label.
 * @returns {HTMLElement[]} An array of length=2 containing element and label.
 */
export function elementWithLabel(elType, elId, labelText) {
    const element = document.createElement(elType);
    const label = document.createElement("label");
    label.htmlFor = elId;
    label.innerHTML = labelText;
    return [element, label];
}

export function getStatic(val) {
    const isDevelopment = process.env.NODE_ENV !== "production";
    if (isDevelopment) {
        // eslint-disable-next-line node/no-deprecated-api
        return url.resolve(window.location.origin, val);
    }
    // eslint-disable-next-line no-undef
    return path.resolve(__static, val);
}
