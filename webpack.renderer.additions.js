const path = require("path");

module.exports = {
    resolve: {
        alias: {
            storyfinder: path.resolve(__dirname, "src/renderer/storyfinder"),
            hotspot: path.resolve(
                __dirname,
                "src/renderer/storyfinder/hotspot"
            ),
            utils: path.resolve(__dirname, "src/renderer/storyfinder/utils.js"),
            maputils: path.resolve(
                __dirname,
                "src/renderer/storyfinder/maputils.js"
            ),
        },
    },
    module: {
        rules: [
            {
                test: /\.md$/,
                use: [
                    {
                        loader: "html-loader",
                    },
                    {
                        loader: "markdown-loader",
                        options: {
                            headerIds: false,
                        },
                    },
                ],
            },
        ],
    },
};
